﻿using TMPro;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    private AudioSource mainMusic;

    public AudioMixer sfxMixer;

    public bool musicOFF = false;
    public bool sfxOFF = false;

    private GameManager _manager;
    // Start is called before the first frame update
    void Start()
    {
        _manager = GetComponentInParent<GameManager>();
        mainMusic = GetComponent<AudioSource>();
        MuteMusic(PlayerPrefs.GetInt("musicOFF",0)==1);
        MuteSFX(PlayerPrefs.GetInt("sfxOFF",0)==1);

        foreach (var button in _manager.menu.muteMusic)
        {
            button.onClick.AddListener(()=>MuteMusic(!musicOFF));
        }
        foreach (var button in _manager.menu.muteSFX)
        {
            button.onClick.AddListener(()=>MuteSFX(!sfxOFF));
        }
    }

    public void MuteMusic(bool mute)
    {
        mainMusic.mute = mute;
        musicOFF = mute;
        if (mute)
        {
            PlayerPrefs.SetInt("musicOFF",1);
            foreach (var button in _manager.menu.muteMusic)
            {
                button.GetComponentInChildren<TextMeshProUGUI>().text = "Enable music";
            }
        }
        else
        {
            PlayerPrefs.SetInt("musicOFF",0);
            
            foreach (var button in _manager.menu.muteMusic)
            {
                button.GetComponentInChildren<TextMeshProUGUI>().text = "Mute music";
            }
        }
    }

    public void MuteSFX(bool mute)
    {
        sfxOFF = mute;
        if (mute)
        {
            PlayerPrefs.SetInt("sfxOFF",1);
            sfxMixer.TransitionToSnapshots(new []{sfxMixer.FindSnapshot("Muted")},new float[]{1},0.1f);
            
            foreach (var button in _manager.menu.muteSFX)
            {
                button.GetComponentInChildren<TextMeshProUGUI>().text = "Enable SFX";
            }
        }
        else
        {
            PlayerPrefs.SetInt("sfxOFF",0);
            sfxMixer.TransitionToSnapshots(new []{sfxMixer.FindSnapshot("Unmuted")},new float[]{1},0.1f);
            
            foreach (var button in _manager.menu.muteSFX)
            {
                button.GetComponentInChildren<TextMeshProUGUI>().text = "Mute SFX";
            }
        }
    }

    [ContextMenu("Apply mute")]
    public void ApplyMute()
    {
        MuteMusic(musicOFF);
        MuteSFX(sfxOFF);
    }
}
