﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;


[CreateAssetMenu(fileName = "Rules", menuName = "Rules", order = 1)]
public class Rules : ScriptableObject
{
    public GameObject poolPrefab;
    public int startScore;
    
    [Header("EndConditions")]
    public int looseScore;
    public int winScore;


    [Header("Balls Setup")] 
    public GameObject aimBall;
    
    public int playerBallsCount;
    public List<GameObject> playerBalls;

    public int obstacleBallsCount;
    public List<GameObject> obstacleBalls;

    
    [Header("Events")]
    
    public GOEvent onPlayerBallScored;
    public GOEvent onAimBallScored;
    public GOEvent onObstacleBallScored;
    
    
    public GOEvent onPlayerBallOut;
    public GOEvent onAimBallOut;
    public GOEvent onObstacleBallOut;
    
    
    public GOEvent onBallShot;

    [Serializable]
    public class GOEvent : UnityEvent<GameObject> {}



    private GameManager _manager;
    public void AddPoints(int points)
    {
        _manager.Score += points;
        if (_manager.Score >= winScore)
        {
            _manager.EndGame(true);
        }else if (_manager.Score <= looseScore)
        {
            _manager.EndGame(false);
        }
    }

    public void Destroy(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }

    public void Respawn(GameObject ball)
    {
        ball.SetActive(false);
        _manager.RespawnBall(ball);
    }

    public void InitGame(GameManager manager)
    {
        _manager = manager;
        _manager.Score = startScore;

        List<Action> spawns = new List<Action>();
        
        for (int i = 0; i < playerBallsCount; i++)
        {
            spawns.Add(()=>_manager.SpawnBall(playerBalls[Random.Range(0,playerBalls.Count)],Ball.BallType.Player));
        }
        for (int i = 0; i < obstacleBallsCount; i++)
        {
            spawns.Add(()=>_manager.SpawnBall(obstacleBalls[Random.Range(0,obstacleBalls.Count)],Ball.BallType.Obstacle));
        }
        _manager.SpawnBall(aimBall,Ball.BallType.Aim);

        spawns = spawns.Shuffle(new System.Random()).ToList();
        foreach (var spawn in spawns)
        {
            spawn.Invoke();
        }
    }
    
}
