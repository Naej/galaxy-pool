﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject pole;
    public LineRenderer aimLine;
    [HideInInspector]
    public GameObject aimBall;
    private GameManager manager;

    [Header("Charging shot parameters")]
    public float maxPower = 100;
    public AnimationCurve charchingCurve = AnimationCurve.EaseInOut(0,0,1,1);
    public float maxChargeTime = 3;
    private float _currentCharge;
    private float _chargeTime;

    [Header("Aiming parameters")] 
    public float keyboardSensibility = 10f;

    [Header("Pole shoot animation parameters")]
    public float animationLength;

    public AnimationCurve animationCurve;

    private Coroutine shoot = null;
    public enum PlayerState
    {
        Init,
        Aiming,
        Waiting
    }

    private PlayerState _state;

    public PlayerState State
    {
        get => _state;
        set
        {
            if (value == PlayerState.Aiming)
            {
                pole.SetActive(true);
                aimLine.enabled = true;
                pole.transform.position = aimBall.transform.position;
            }
            _state = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        manager = GetComponentInParent<GameManager>();
        manager.input.whileActionPressed.AddListener(OnActionPressed);
        manager.input.onMouseMove.AddListener(OnMouseMove);
        manager.input.onKeyboardMove.AddListener(OnKeyBoardMove);
        manager.input.onActionUp.AddListener(OnActionReleased);

        Reset();
    }
    
    public void OnMouseMove(Vector3 mousePos)
    {
        if (!enabled || _state != PlayerState.Aiming) return;
        pole.transform.position = aimBall.transform.position + Vector3.ClampMagnitude(mousePos - aimBall.transform.position,2);
        pole.transform.up =  aimBall.transform.position - pole.transform.position;
        pole.transform.rotation = Quaternion.Euler(0,0,pole.transform.rotation.eulerAngles.z);
        
    }
    public void OnKeyBoardMove(float move)
    {
        if (!enabled || _state != PlayerState.Aiming) return;

        pole.transform.position = aimBall.transform.position;
        pole.transform.rotation = Quaternion.Euler(0,0,pole.transform.rotation.eulerAngles.z + Time.deltaTime * move * keyboardSensibility);
    }

    public void OnActionPressed()
    {
        if(!enabled || _state != PlayerState.Aiming) return;
        manager.menu.PowerGauge.SetActive(true);
        
        pole.transform.position = aimBall.transform.position;
        _chargeTime += Time.deltaTime;
        _currentCharge = charchingCurve.Evaluate(_chargeTime / maxChargeTime) * maxPower;
        manager.menu.PowerGaugeFill.fillAmount = _currentCharge / maxPower;
    }

    public void OnActionReleased()
    {
        if(!enabled || _state != PlayerState.Aiming) return;
        if (_currentCharge < 0.1f)
        {
            _currentCharge = 0;
            _chargeTime = 0;
            return;
        }
        State = PlayerState.Waiting;
        manager.menu.PowerGauge.SetActive(false);
        aimLine.enabled = false;
        shoot = StartCoroutine(Shoot(aimBall.GetComponent<Rigidbody2D>(), _currentCharge));
        _currentCharge = 0;
        _chargeTime = 0;
    }

    public void Reset()
    {
        State = PlayerState.Aiming;
        _chargeTime = 0;
        _currentCharge = 0;
        if(shoot != null) StopCoroutine(shoot);
    }

    //Shoots the ball and then waits until the balls are at rest before going back into aiming mode
    public IEnumerator Shoot(Rigidbody2D ball, float force)
    {
        pole.transform.position = ball.transform.position + pole.transform.up * 0.5f;

        for (float i = 0; i < animationLength; i+= Time.fixedDeltaTime)
        {
            pole.transform.position = ball.transform.position -
                                      pole.transform.up * ((animationCurve.Evaluate(i / animationLength) * force / 200f)-0.5f);
            yield return new WaitForFixedUpdate();
        }
        pole.transform.position = ball.transform.position + pole.transform.up * 0.5f;
        ball.AddForce(pole.transform.up * force);
        manager.OnBallShot(aimBall);
        yield return new WaitForSeconds(0.5f);
        pole.SetActive(false);

        yield return manager.waitForRestState();

        if (enabled && State == PlayerState.Waiting)
        {
            State = PlayerState.Aiming;
        }
    }

    private void OnDisable()
    {
        _chargeTime = 0;
        _currentCharge = 0;
        pole.SetActive(false);
    }
}
