﻿using UnityEngine;

public class SelfDestroy : MonoBehaviour
{
    public float delay = 1f;
    // Start is called before the first frame update
    private void Start()
    {
        Invoke(nameof(DestroySelf),delay);
    }

    private void DestroySelf()
    {
        Destroy(gameObject);
    }
    
}
