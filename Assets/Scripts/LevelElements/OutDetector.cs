﻿using UnityEngine;

public class OutDetector : MonoBehaviour
{
    public void OnTriggerExit2D(Collider2D other)
    {
        if(!other.gameObject.activeSelf || other.GetComponent<Ball>() == null) return; //avoid detecting scored balls
        
        other.gameObject.SetActive(false);
        GetComponentInParent<GameManager>().OnBallOut(other.gameObject);
    }
}
