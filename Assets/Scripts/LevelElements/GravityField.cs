﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityField : MonoBehaviour
{
    public float strength = 10;
    public void OnTriggerStay2D(Collider2D other)
    {
        Rigidbody2D ballRb = other.attachedRigidbody;
        if (ballRb == null) return;

        float dist = (ballRb.transform.position - transform.position).magnitude;
        
        if (dist < transform.lossyScale.x)
        {
            Vector3 offset = transform.position - ballRb.transform.position;
            ballRb.AddForce( offset / offset.sqrMagnitude * ballRb.mass * strength);
        }
    }
}
