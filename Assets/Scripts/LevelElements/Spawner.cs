﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<Ball.BallType> balls;
    public float respawnArea = 4;
    public LayerMask checkMask;

    private int _spawnedBalls = 0;

    public GameObject SpawnBall(GameObject ball, Ball.BallType type)
    {
        if (!balls.Contains(type)) return null;

        GameObject spawned = Instantiate(ball, transform);
        spawned.transform.position = transform.position + 
                                     _spawnedBalls * 0.25f * -transform.right +
                                     ((_spawnedBalls+2)%4 -2) * transform.up;
        spawned.GetComponent<Ball>().type = type;
        _spawnedBalls++;
        return spawned;
    }

    public GameObject RespawnBall(GameObject ball)
    {
        Vector3 pos = transform.position + Random.insideUnitSphere * respawnArea;
        pos.z = 0;
        if (Physics2D.OverlapCircle(pos, 1.5f,checkMask) == null)
        {
            ball.transform.position = pos;
            ball.SetActive(true);
            return ball;
        }
        else
        {
            return null;
        }
    }
}
