﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Hole : MonoBehaviour
{
    public float attractionDistance = 0.5f;
    public float scoreDistance = 0.1f;

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.GetComponent<Ball>() == null) return;

        Rigidbody2D ballRb = other.attachedRigidbody;
        float dist = (ballRb.transform.position - transform.position).magnitude;
        
        if (dist < scoreDistance * transform.lossyScale.x)
        {
            other.gameObject.SetActive(false);
            other.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponentInParent<GameManager>().OnBallScored(other.gameObject);
        }else if (dist < attractionDistance * transform.lossyScale.x) // simulates the ball on the edge of the hole
        {
            Vector3 offset = transform.position - ballRb.transform.position;
            ballRb.AddForce( offset / offset.sqrMagnitude * ballRb.mass * 10);
        }
    }
}
