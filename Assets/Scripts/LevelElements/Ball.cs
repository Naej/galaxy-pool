﻿
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour
{
    public enum BallType
    {
        Player,
        Aim,
        Obstacle
    }

    public BallType type;
    public List<GameObject> FX;
    
    // Start is called before the first frame update
    private void Start()
    {
        GetComponentInParent<GameManager>().RegisterBall(GetComponent<Rigidbody2D>());
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.relativeVelocity.magnitude > 1f)
            Instantiate(FX[Random.Range(0,FX.Count)]).transform.position = other.contacts[0].point;
    }
}
