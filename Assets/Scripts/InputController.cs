﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputController : MonoBehaviour
{
    public enum InputSheme
    {
        Keyboard,
        Mouse
    }

    public InputSheme currentSheme;

    [Serializable]
    public class MouseMove : UnityEvent<Vector3>{}
    [Serializable]
    public class KeyboardMove : UnityEvent<float>{}
    
    public MouseMove onMouseMove;
    public KeyboardMove onKeyboardMove;
    public UnityEvent onActionDown;
    public UnityEvent whileActionPressed;
    public UnityEvent onActionUp;

    public bool actionDown = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentSheme == InputSheme.Mouse)
        {
            if (Input.mousePresent)
            {
                onMouseMove.Invoke(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }

            if (Input.GetMouseButton(0) != actionDown)
            {
                actionDown = !actionDown;
                
                if(actionDown) onActionDown.Invoke();
                else onActionUp.Invoke();
                
            }else if (actionDown)
            {
                whileActionPressed.Invoke();
            }
        }else if (currentSheme == InputSheme.Keyboard)
        {
            onKeyboardMove.Invoke(Input.GetAxis("Horizontal"));

            if (Input.GetButton("Submit") != actionDown)
            {
                actionDown = !actionDown;
                
                if(actionDown) onActionDown.Invoke();
                else onActionUp.Invoke();
                
            }else if (actionDown)
            {
                whileActionPressed.Invoke();
            }
            
        }
    }
}
