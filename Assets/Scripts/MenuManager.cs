﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public EventSystem eventSystem;
    [HideInInspector]
    public GameManager manager;
    
    
    [Header("Main menu")]
    public GameObject MainMenu;

    public Button KeyboardInput;
    public Button MouseInput;


    [Header("GameMode")] 
    public GameObject GameModeMenu;

    [Serializable]
    public struct GameModeButton
    {
        public Button button;
        public Rules mode;
    }

    public List<GameModeButton> modes;
    
    [Header("HUD")] 
    public GameObject HUD;

    public TextMeshProUGUI score;
    public GameObject PowerGauge;
    public Image PowerGaugeFill;
    
    [Header("End")] 
    public GameObject winMenu;
    public GameObject winMenuStartItem;
    public GameObject looseMenu;
    public GameObject looseMenuStartItem;

    public List<Button> restart;
    public List<Button> goToMain;

    [Header("Options")] 
    
    public List<Button> muteMusic;
    public List<Button> muteSFX;
    
    
    // Start is called before the first frame update
    void Start()
    {
        InitButtons();
        
        ShowMain();
    }

    public void InitButtons()
    {
        KeyboardInput.onClick.AddListener(()=>
        {
            manager.input.currentSheme = InputController.InputSheme.Keyboard;
            ShowGameMode();
        });
        MouseInput.onClick.AddListener(() =>
        {
            manager.input.currentSheme = InputController.InputSheme.Mouse;
            ShowGameMode();
        });

        foreach (var mode in modes)
        {
            mode.button.onClick.AddListener(() =>
            {
                ShowHUD();
                manager.InitGame(mode.mode);
            });
        }


        foreach (var button in restart)
        {
            button.onClick.AddListener(()=>
            {
                manager.InitGame(manager.currentRules);
                ShowHUD();
            });
        }
        foreach (var button in goToMain)
        {
            button.onClick.AddListener(()=>
            {
                manager.ClearGame();
                ShowMain();
            });
        }
    }

    public void HideAllMenus()
    {
        MainMenu.gameObject.SetActive(false);
        GameModeMenu.gameObject.SetActive(false);
        HUD.SetActive(false);
        looseMenu.SetActive(false);
        winMenu.SetActive(false);
    }

    public void ShowMain()
    {
        HideAllMenus();
        MainMenu.SetActive(true);      
        eventSystem.SetSelectedGameObject(KeyboardInput.gameObject);

    }

    public void ShowGameMode()
    {
        HideAllMenus();
        GameModeMenu.SetActive(true);
        eventSystem.SetSelectedGameObject(modes[0].button.gameObject);
    }

    public void ShowHUD()
    {
        HideAllMenus();
        HUD.SetActive(true);
        PowerGauge.SetActive(false);
    }

    public void ShowGameOver()
    {
        HideAllMenus();
        looseMenu.SetActive(true);
        eventSystem.SetSelectedGameObject(looseMenuStartItem);
    }
    public void ShowWin()
    {
        HideAllMenus();
        winMenu.SetActive(true);
        eventSystem.SetSelectedGameObject(winMenuStartItem);

    }
}
