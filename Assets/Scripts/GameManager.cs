﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public InputController input;

    public MenuManager menu;
    public PlayerController player;

    private GameObject level;

    [Header("Game data")] 
    public Rules currentRules;
    
    private int _score;
    private List<Rigidbody2D> balls = new List<Rigidbody2D>();
    private List<Spawner> spawners = new List<Spawner>();

    private List<GameObject> toRespawn = new List<GameObject>();


    public bool isPlaying = false;
    public GameObject scoreFX;
    public GameObject destroyFX;

    public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            menu.score.text = Score + "";
        }
    }


    void Awake()
    {
        input = GetComponent<InputController>();
        menu.manager = this;
    }

    public void InitGame(Rules rules)
    {
        ClearGame();
        
        currentRules = rules;
        level = Instantiate(currentRules.poolPrefab, transform);
        CollectSpawners();
        currentRules.InitGame(this);
        
        player.gameObject.SetActive(true);
        player.enabled = true;
        player.Reset();
        isPlaying = true;
    }

    public void ClearGame()
    {
        isPlaying = false;
        if (level != null)
        {
            Destroy(level);
        }
        balls.Clear();
        spawners.Clear();
        toRespawn.Clear();
        Score = 0;
    }

    public void OnBallShot(GameObject ball)
    {
        currentRules.onBallShot.Invoke(ball);
    }

    public void OnBallScored(GameObject ball)
    {
        Ball currentBall = ball.GetComponent<Ball>();
        if (currentBall == null) return;

        Instantiate(scoreFX).transform.position = ball.transform.position;
        switch (currentBall.type)
        {
            case Ball.BallType.Aim:
                currentRules.onAimBallScored.Invoke(ball);
                break;
            case Ball.BallType.Obstacle:
                currentRules.onObstacleBallScored.Invoke(ball);
                break;
            case Ball.BallType.Player:
                currentRules.onPlayerBallScored.Invoke(ball);
                break;
        }
    }

    public void OnBallOut(GameObject ball)
    {
        Ball currentBall = ball.GetComponent<Ball>();
        if (currentBall == null || !isPlaying) return;

        Instantiate(destroyFX).transform.position = ball.transform.position;

        switch (currentBall.type)
        {
            case Ball.BallType.Aim:
                currentRules.onAimBallOut.Invoke(ball);
                break;
            case Ball.BallType.Obstacle:
                currentRules.onObstacleBallOut.Invoke(ball);
                break;
            case Ball.BallType.Player:
                currentRules.onPlayerBallOut.Invoke(ball);
                break;
        }
    }

    public void RespawnBall(GameObject ball)
    {
        toRespawn.Add(ball);
    }
    
    IEnumerator TryRespawnBall(GameObject ball)
    {
        bool succes = false;
        while (!succes)
        {
            yield return new WaitForFixedUpdate();
            spawners.Sort((a, b) => Random.Range(-1, 1));
            foreach (var spawner in spawners)
            {
                if (ball != null && spawner.RespawnBall(ball) != null)
                {
                    succes = true;
                    break;
                }
            }
        }
    }

    public void RegisterBall(Rigidbody2D ballRb)
    {
        balls.Add(ballRb);
    }

    public bool areBallsAtRest()
    {
        float sum = 0;
        foreach (var ball in balls)
        {
            float speed = ball.velocity.magnitude;
            if (speed < 0.01f)
            {
                speed = 0f;
                ball.velocity = Vector2.zero;                
            }
            sum += speed;
        }

        return sum < 0.05f;
    }

    //used by the playerController to know when the pool is ready for an other shot
    public IEnumerator waitForRestState()
    {
        while (!areBallsAtRest())
        {
            yield return new WaitForFixedUpdate();
        }
        // respawn the stuff
        foreach (var ball in toRespawn)
        {
            yield return TryRespawnBall(ball);
        }
        toRespawn.Clear();
        
    }
    
    public void CollectSpawners()
    {
        spawners = level.GetComponentsInChildren<Spawner>().ToList();
    }
    public void SpawnBall(GameObject ball, Ball.BallType type)
    {
        spawners = spawners.Shuffle(new System.Random(Random.Range(0,100))).ToList();
        foreach (var spawner in spawners)
        {
            if (type == Ball.BallType.Aim)
            {
                GameObject aim = spawner.SpawnBall(ball, type);
                if (aim != null)
                {
                    player.aimBall = aim;
                    return;
                }
            }else if(spawner.SpawnBall(ball,type) != null) return;
        }
    }

    public void EndGame(bool win)
    {
        player.enabled = false;
        player.gameObject.SetActive(false);
        isPlaying = false;
        if(win)
            menu.ShowWin();
        else
            menu.ShowGameOver();
    }

}
